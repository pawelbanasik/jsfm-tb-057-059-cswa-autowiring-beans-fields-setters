package com.pawelbanasik;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.pawelbanasik.domain.promotion.TradeFair;

public class AutowiredAnnotationApp {
	public static void main(String[] args) {

		ApplicationContext context = new FileSystemXmlApplicationContext("beans.xml");
		
		TradeFair tradeFair = (TradeFair) context.getBean("myfair");
		
		System.out.println(tradeFair.specialPromotionalPricing());
		
		((AbstractApplicationContext) context).close();
		
		
	}
}
